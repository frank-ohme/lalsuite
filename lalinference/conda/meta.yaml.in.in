# This is a copy of the upstream conda recipe for this package
# hosted at github.com/conda-forge/lal-feedstock and licensed
# under the terms of the BSD 3-Clause license.
# This is only for CI purposes, and should not be considered
# production-ready at any time.

{% set name = "lalinference" %}
{% set version = "@VERSION@".replace('-', '.') %}

# dependencies
{% set lal_version = "@MIN_LAL_VERSION@a0" %}
{% set lalburst_version = "@MIN_LALBURST_VERSION@a0" %}
{% set lalframe_version = "@MIN_LALFRAME_VERSION@a0" %}
{% set lalinspiral_version = "@MIN_LALINSPIRAL_VERSION@a0" %}
{% set lalmetaio_version = "@MIN_LALMETAIO_VERSION@a0" %}
{% set lalpulsar_version = "@MIN_LALPULSAR_VERSION@a0" %}
{% set lalsimulation_version = "@MIN_LALSIMULATION_VERSION@a0" %}
{% set swig_version = "@MIN_SWIG_VERSION@" %}
{% set numpy_version = "@MIN_NUMPY_VERSION@" %}

package:
  name: {{ name }}-split
  version: {{ version }}

source:
  url: file://@TARBALL@
  sha256: @SHA256@

build:
  error_overdepending: true
  error_overlinking: true
  number: 9999
  script_env:
    - CI_PIPELINE_SOURCE
  skip: true  # [win]

requirements:
  build:
    - {{ compiler('c') }}
    - help2man >=1.37
    - llvm-openmp  # [osx]
    - make
    - pkg-config
    - swig >={{ swig_version }}
  host:
    - gsl
    - lal >={{ lal_version }} fftw*
    - lalframe >={{ lalframe_version }}
    - lalmetaio >={{ lalmetaio_version }}
    - lalsimulation >={{ lalsimulation_version }}
    - lalburst >={{ lalburst_version }}
    - lalinspiral >={{ lalinspiral_version }}
    - lalpulsar >={{ lalpulsar_version }}
    - libgomp  # [linux]
    - llvm-openmp  # [osx]
    - openmpi

outputs:
  - name: liblalinference
    script: install-lib.sh
    build:
      error_overdepending: true
      error_overlinking: true
      run_exports:
        - {{ pin_subpackage("liblalinference", max_pin="x") }}
    requirements:
      build:
        - {{ compiler('c') }}
        - llvm-openmp  # [osx]
        - make
        - swig >={{ swig_version }}
      host:
        - gsl
        - libgomp  # [linux]
        - liblal >={{ lal_version }} fftw*
        - liblalframe >={{ lalframe_version }}
        - liblalmetaio >={{ lalmetaio_version }}
        - liblalsimulation >={{ lalsimulation_version }}
        - liblalburst >={{ lalburst_version }}
        - liblalinspiral >={{ lalinspiral_version }}
        - liblalpulsar >={{ lalpulsar_version }}
        - llvm-openmp  # [osx]
      run:
        - gsl
        - liblal >={{ lal_version }} fftw*
        - liblalframe >={{ lalframe_version }}
        - liblalmetaio >={{ lalmetaio_version }}
        - liblalsimulation >={{ lalsimulation_version }}
        - liblalburst >={{ lalburst_version }}
        - liblalinspiral >={{ lalinspiral_version }}
        - liblalpulsar >={{ lalpulsar_version }}
        - llvm-openmp  # [osx]

  - name: python-lalinference
    script: install-python.sh
    build:
      error_overdepending: true
      error_overlinking: true
      ignore_run_exports:
        - openmpi
        # ignore run_exports from python's recipe
        - python
    requirements:
      build:
        - {{ compiler('c') }}
        - make
        - pkg-config
        - swig >={{ swig_version }}
      host:
        - gsl
        - {{ pin_subpackage('liblalinference', exact=True) }}
        - openmpi
        - python
        - numpy
        # run requirements needed for help2man
        - astropy >=1.1.1
        - healpy >=1.9.1
        - h5py
        - ligo-segments
        - lscsoft-glue >=1.54.1
        - matplotlib-base >=1.2.0
        - python-lal >={{ lal_version }}
        - python-lalmetaio >={{ lalmetaio_version }}
        - python-lalsimulation >={{ lalsimulation_version }}
        - python-lalburst >={{ lalburst_version }}
        - python-lalinspiral >={{ lalinspiral_version }}
        - python-lalpulsar >={{ lalpulsar_version }}
        - python-ligo-lw
        - scipy >=0.9.0
      run:
        - astropy >=1.1.1
        - gsl
        - healpy >=1.9.1
        - h5py
        - liblal >={{ lal_version }}
        - liblalburst >={{ lalburst_version }}  # [linux]
        - liblalframe >={{ lalframe_version }}  # [linux]
        - {{ pin_subpackage('liblalinference', exact=True) }}
        - liblalinspiral >={{ lalinspiral_version }}  # [linux]
        - liblalmetaio >={{ lalmetaio_version }}  # [linux]
        - liblalpulsar >={{ lalpulsar_version }}  # [linux]
        - liblalsimulation >={{ lalsimulation_version }}  # [linux]
        - ligo-segments
        - lscsoft-glue >=1.54.1
        - matplotlib-base >=1.2.0
        - {{ pin_compatible('numpy') }}
        - python
        - python-lal >={{ lal_version }}
        - python-lalmetaio >={{ lalmetaio_version }}
        - python-lalsimulation >={{ lalsimulation_version }}
        - python-lalburst >={{ lalburst_version }}
        - python-lalinspiral >={{ lalinspiral_version }}
        - python-lalpulsar >={{ lalpulsar_version }}
        - python-ligo-lw
        - scipy >=0.9.0
    test:
      requires:
        - pytest >=4.0.0a0
      source_files:
        - test/python
      commands:
        - python -m pytest -rs -v --junit-xml=junit.xml test/python/
      imports:
        - lalinference
        - lalinference.bayespputils
        - lalinference.imrtgr
        - lalinference.imrtgr.imrtgrutils
        - lalinference.imrtgr.nrutils
        - lalinference.imrtgr.pneqns
        - lalinference.lalinference_pipe_utils
        - lalinference.nest2pos
        - lalinference.plot
        - lalinference.tiger
        - lalinference.tiger.make_injtimes
        - lalinference.tiger.omegascans_dag
        - lalinference.tiger.postproc
        - lalinference.wrapper
    about:
      home: https://wiki.ligo.org/Computing/LALSuite
      doc_url: https://docs.ligo.org/lscsoft/lalsuite/lalinference/
      dev_url: https://git.ligo.org/lscsoft/lalsuite/
      license: GPL-2.0-or-later
      license_family: GPL
      license_file: COPYING
      summary: LSC Algorithm Inference Library
      description: |
        The LSC Algorithm Inference Library for gravitational wave data
        analysis.  This package contains the python bindings.

  - name: lalinference
    script: install-bin.sh
    build:
      ignore_run_exports:
        - python
    requirements:
      build:
        - {{ compiler('c') }}
        - help2man
        - llvm-openmp  # [osx]
        - make
      host:
        - gsl
        - {{ pin_subpackage('liblalinference', exact=True) }}
        - libgomp  # [linux]
        - llvm-openmp  # [osx]
        - openmpi
        - python
        - {{ pin_subpackage('python-lalinference', exact=True) }}
      run:
        - gsl
        - liblal >={{ lal_version }}
        - {{ pin_subpackage('liblalinference', exact=True) }}
        - liblalinspiral >={{ lalinspiral_version }}
        - llvm-openmp  # [osx]
        - openmpi
        - python
        - {{ pin_subpackage('python-lalinference', exact=True) }}
    test:
      requires:
        - cpnest
        - openssh
      commands:
        # data files
        - test -f "${LALINFERENCE_DATADIR}/lalinference_pipe_example.ini"  # [unix]
        # C executables
        - lalinference_bench --psdlength 1000 --psdstart 1 --seglen 8 --srate 4096 --trigtime 0 --ifo H1 --H1-channel LALSimAdLIGO --H1-cache LALSimAdLIGO --dataseed 1324 --Niter 10 --fix-chirpmass 1.21
        #- lalinference_burst --help
        - lalinference_datadump --help
        - lalinference_injectedlike --help
        - lalinference_kombine --help
        - lalinference_mcmc --help
        - lalinference_nest --help
        - lalinference_version --verbose
        # python scripts
        - cbcBayesBurstPPAnalysis --help
        - cbcBayesBurstPostProc --help
        - cbcBayesCombinePTMCMCh5s --help
        - cbcBayesCombinePosteriors --help
        - cbcBayesCompPos --help
        - cbcBayesDIEvidence --help
        - cbcBayesGraceDBinfo --help
        - cbcBayesMCMC2pos --help
        - cbcBayesPPAnalysis --help
        - cbcBayesPlotSpinDisk --help
        - cbcBayesPosToSimBurst --help
        - cbcBayesPosToSimInspiral --help
        - cbcBayesPostProc --help
        - cbcBayesThermoInt --help
        - imrtgr_imr_consistency_test --help
        - lalinference_burst_pp_pipe --help
        - lalinference_coherence_test --help
        - lalinference_compute_roq_weights --help
        - lalinference_cpnest --help
        - lalinference_merge_posteriors --help
        - lalinference_multi_pipe --help
        - lalinference_nest2pos --help
        - lalinference_pipe --help
        - lalinference_pp_pipe --help
        - lalinference_review_test --help
      imports:
        - lalinference
    about:
      home: https://wiki.ligo.org/Computing/LALSuite
      doc_url: https://docs.ligo.org/lscsoft/lalsuite/lalinference/
      dev_url: https://git.ligo.org/lscsoft/lalsuite/
      license: GPL-2.0-or-later
      license_family: GPL
      license_file: COPYING
      summary: LSC Algorithm Inference Library
      description: |
        The LSC Algorithm Inference Library for gravitational wave data analysis.

about:
  home: https://wiki.ligo.org/Computing/LALSuite
  doc_url: https://docs.ligo.org/lscsoft/lalsuite/lalinference/
  dev_url: https://git.ligo.org/lscsoft/lalsuite/
  license: GPL-2.0-or-later
  license_family: GPL
  license_file: COPYING
  summary: LSC Algorithm Inference Library
  description: |
    The LSC Algorithm Inference Library for gravitational wave data analysis.

extra:
  feedstock-name: lalinference
  recipe-maintainers:
    - duncanmmacleod
    - skymoo
    - vivienr
