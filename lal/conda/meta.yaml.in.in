# This is a copy of the upstream conda recipe for this package
# hosted at github.com/conda-forge/lal-feedstock and licensed
# under the terms of the BSD 3-Clause license.
# This is only for CI purposes, and should not be considered
# production-ready at any time.

{% set name = "lal" %}
{% set version = "@VERSION@".replace('-', '.') %}

{% set swig_version = "@MIN_SWIG_VERSION@" %}
{% set numpy_version = "@MIN_NUMPY_VERSION@" %}

package:
  name: {{ name }}-split
  version: {{ version }}

source:
  url: file://@TARBALL@
  sha256: @SHA256@

build:
  error_overdepending: true
  error_overlinking: true
  ignore_run_exports:
    - blas
    # run_exports parsing for fftw is broken, so we ignore it
    # manually, for now
    - fftw
  number: 9999
  script_env:
    - CI_PIPELINE_SOURCE
  skip: true  # [win]

requirements:
  build:
    - {{ compiler('c') }}
    - pkg-config >=0.18.0
    - make
    - help2man >=1.37
    - bc
    - swig >={{ swig_version }}
  host:
    - fftw * nompi*  # [fft_impl == "fftw"]
    - gsl
    - hdf5
    - mkl-devel {{ mkl }}  # [fft_impl == "mkl"]
    - zlib

outputs:
  - name: liblal
    script: install-lib.sh
    build:
      run_exports:
        - {{ pin_subpackage("liblal", max_pin="x") }}
      string: {{ fft_impl }}_h{{ PKG_HASH }}_{{ PKG_BUILDNUM }}
    requirements:
      build:
        - {{ compiler('c') }}
        - make  # [not win]
        - swig >=3.0.9
      host:
        - fftw * nompi*  # [fft_impl == "fftw"]
        - gsl
        - hdf5
        - mkl-devel {{ mkl }}  # [fft_impl == "mkl"]
        - zlib
      run:
        - fftw  # [fft_impl == "fftw"]
        - gsl
        - hdf5
        - zlib
    about:
      home: https://wiki.ligo.org/Computing/LALSuite
      doc_url: https://lscsoft.docs.ligo.org/lalsuite/lal/
      dev_url: https://git.ligo.org/lscsoft/lalsuite/
      license: GPL-2.0-or-later
      license_family: GPL
      license_file: COPYING
      summary: LSC Algorithm Library shared object libraries
      description: |
        The LSC Algorithm Library for gravitational wave data analysis.
        This package contains the shared object libraries need to build
        applications that use LAL.
    test:
      requires:
        - pkg-config
      commands:
        - test $(pkg-config --modversion --print-errors lal) == "@VERSION@"
        - test $(pkg-config --modversion --print-errors lalsupport) == "@VERSION@"

  - name: python-lal
    script: install-python.sh
    build:
      error_overdepending: true
      error_overlinking: true
      ignore_run_exports:
        # ignore run_exports from python's recipe
        - python
      string: {{ fft_impl }}_py{{ py }}h{{ PKG_HASH }}_{{ PKG_BUILDNUM }}
    requirements:
      build:
        - {{ compiler('c') }}
        - make
        - pkg-config >=0.18.0
        - swig >={{ swig_version }}
      host:
        - gsl
        - {{ pin_subpackage('liblal', exact=True) }}
        - mkl-devel {{ mkl }}  # [fft_impl == "mkl"]
        - numpy
        - python
      run:
        # NOTE: on linux, python-lal links the LAL dependencies
        #       as well so we have to list them here. why this
        #       doesn't happen on osx, I'm not sure.
        - fftw  # [linux and fft_impl == "fftw"]
        - gsl
        - hdf5  # [linux]
        - {{ pin_subpackage('liblal', exact=True) }}
        - python-ligo-lw
        - ligo-segments
        - lscsoft-glue >=@MIN_GLUE_VERSION@
        - mkl  # [fft_impl == "mkl"]
        - {{ pin_compatible('numpy') }}
        - numpy <1.20.0a0
        - python
        - python-dateutil
        - scipy
        - six
        - zlib  # [linux]
    test:
      requires:
        - freezegun
        - mock  # [py<33]
        - pathlib  # [py<34]
        - pytest >=4.0.0a0
      source_files:
        - test/python
      commands:
        - python -m pytest -rs -v --junit-xml=junit.xml test/python
      imports:
        - lal
        - lal.antenna
        - lal.gpstime
        - lal.rate
        - lal.series
        - lal.utils
    about:
      home: https://wiki.ligo.org/Computing/LALSuite
      doc_url: https://lscsoft.docs.ligo.org/lalsuite/lal/
      dev_url: https://git.ligo.org/lscsoft/lalsuite/
      license: GPL-2.0-or-later
      license_family: GPL
      license_file: COPYING
      summary: LSC Algorithm Library Python bindings
      description: |
        The LSC Algorithm Library for gravitational wave data analysis.
        This package contains the Python bindings.

  - name: lal
    script: install-bin.sh
    build:
      error_overdepending: true
      error_overlinking: true
      ignore_run_exports:
        - python
      string: {{ fft_impl }}_py{{ py }}h{{ PKG_HASH }}_{{ PKG_BUILDNUM }}
    requirements:
      build:
        - {{ compiler('c') }}
        - make
      host:
        - {{ pin_subpackage('liblal', exact=True) }}
        # this is here in case the solver gets confused during conda-build
        - mkl {{ mkl }}  # [fft_impl == "mkl"]
        - python
      run:
        - {{ pin_subpackage('liblal', exact=True) }}
        - python
        - {{ pin_subpackage('python-lal', exact=True) }}
    about:
      home: https://wiki.ligo.org/Computing/LALSuite
      doc_url: https://lscsoft.docs.ligo.org/lalsuite/lal/
      dev_url: https://git.ligo.org/lscsoft/lalsuite/
      license: GPL-2.0-or-later
      license_family: GPL
      license_file: COPYING
      summary: LSC Algorithm Library runtime tools
      description: |
        The LSC Algorithm Library for gravitational wave data analysis.
        This package contains the runtime tools.
    test:
      commands:
        - lal_version --verbose
        - lal_simd_detect
      imports:
        - lal

about:
  home: https://wiki.ligo.org/Computing/LALSuite
  doc_url: https://lscsoft.docs.ligo.org/lalsuite/lal/
  dev_url: https://git.ligo.org/lscsoft/lalsuite/
  license: GPL-2.0-or-later
  license_family: GPL
  license_file: COPYING
  summary: LSC Algorithm Library
  description: |
    The LSC Algorithm Library for gravitational wave data analysis.
    This package contains the shared-object libraries needed to run
    applications that use the LAL library.  If you want to install
    the Python bindings, please install the associated python-lal
    package.

extra:
  feedstock-name: lal
  recipe-maintainers:
    - duncanmmacleod
    - skymoo
